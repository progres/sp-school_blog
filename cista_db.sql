-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Počítač: 127.0.0.1
-- Vytvořeno: Čtv 20. lis 2014, 00:07
-- Verze serveru: 5.6.20
-- Verze PHP: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databáze: `school`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `article_n_tagy`
--

CREATE TABLE IF NOT EXISTS `article_n_tagy` (
  `tagy_id` int(11) NOT NULL,
  `clanky_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

-- --------------------------------------------------------

--
-- Struktura tabulky `clanky`
--

CREATE TABLE IF NOT EXISTS `clanky` (
`id` int(11) NOT NULL,
  `uzivatele_id` int(11) DEFAULT NULL,
  `text` text COLLATE utf8_czech_ci,
  `date` datetime DEFAULT NULL,
  `nadpis` varchar(200) COLLATE utf8_czech_ci DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `clanky_rating`
--

CREATE TABLE IF NOT EXISTS `clanky_rating` (
`id` int(11) NOT NULL,
  `rating` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `clanky_id` int(11) NOT NULL,
  `uzivatele_id` int(11) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `coment_rating`
--

CREATE TABLE IF NOT EXISTS `coment_rating` (
`id` int(11) NOT NULL,
  `value` int(11) DEFAULT NULL,
  `uzivatele_id` int(11) DEFAULT NULL,
  `komentare_id` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `komentare`
--

CREATE TABLE IF NOT EXISTS `komentare` (
`id` int(11) NOT NULL,
  `uzivatele_id` int(11) NOT NULL,
  `clanky_id` int(11) NOT NULL,
  `date` datetime DEFAULT NULL,
  `text` text COLLATE utf8_czech_ci
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `tagy`
--

CREATE TABLE IF NOT EXISTS `tagy` (
`id` int(11) NOT NULL,
  `nazev` varchar(45) COLLATE utf8_czech_ci DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=20 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `uzivatele`
--

CREATE TABLE IF NOT EXISTS `uzivatele` (
`id` int(11) NOT NULL,
  `username` varchar(100) COLLATE utf8_czech_ci DEFAULT NULL,
  `password` varchar(100) COLLATE utf8_czech_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `fb_id` int(11) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=26 ;

--
-- Vypisuji data pro tabulku `uzivatele`
--

INSERT INTO `uzivatele` (`id`, `username`, `password`, `email`, `date`, `fb_id`) VALUES
(26, 'eee', '8578173555a47d4ea49e697badfda270dee0858f', 'fdsf@fdsf.cgds', '2014-11-19 23:55:21', NULL);

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `article_n_tagy`
--
ALTER TABLE `article_n_tagy`
 ADD PRIMARY KEY (`tagy_id`,`clanky_id`), ADD KEY `fk_tagy_has_Articles_Articles1_idx` (`clanky_id`), ADD KEY `fk_tagy_has_Articles_tagy1_idx` (`tagy_id`);

--
-- Klíče pro tabulku `clanky`
--
ALTER TABLE `clanky`
 ADD PRIMARY KEY (`id`), ADD KEY `uzivatele_id` (`uzivatele_id`);

--
-- Klíče pro tabulku `clanky_rating`
--
ALTER TABLE `clanky_rating`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_article_rating_Articles1_idx` (`clanky_id`), ADD KEY `fk_article_rating_uzivatele1_idx` (`uzivatele_id`);

--
-- Klíče pro tabulku `coment_rating`
--
ALTER TABLE `coment_rating`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_coment_rating_uzivatele1_idx` (`uzivatele_id`), ADD KEY `fk_coment_rating_komentare1_idx` (`komentare_id`);

--
-- Klíče pro tabulku `komentare`
--
ALTER TABLE `komentare`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_komentare_uzivatele1_idx` (`uzivatele_id`), ADD KEY `clanky_id` (`clanky_id`);

--
-- Klíče pro tabulku `tagy`
--
ALTER TABLE `tagy`
 ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `uzivatele`
--
ALTER TABLE `uzivatele`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `username_UNIQUE` (`username`), ADD UNIQUE KEY `email_UNIQUE` (`email`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `clanky`
--
ALTER TABLE `clanky`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT pro tabulku `clanky_rating`
--
ALTER TABLE `clanky_rating`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pro tabulku `coment_rating`
--
ALTER TABLE `coment_rating`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pro tabulku `komentare`
--
ALTER TABLE `komentare`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pro tabulku `tagy`
--
ALTER TABLE `tagy`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT pro tabulku `uzivatele`
--
ALTER TABLE `uzivatele`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- Omezení pro exportované tabulky
--

--
-- Omezení pro tabulku `article_n_tagy`
--
ALTER TABLE `article_n_tagy`
ADD CONSTRAINT `article_n_tagy_ibfk_4` FOREIGN KEY (`clanky_id`) REFERENCES `clanky` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `article_n_tagy_ibfk_5` FOREIGN KEY (`tagy_id`) REFERENCES `tagy` (`id`) ON UPDATE CASCADE;

--
-- Omezení pro tabulku `clanky`
--
ALTER TABLE `clanky`
ADD CONSTRAINT `clanky_ibfk_2` FOREIGN KEY (`uzivatele_id`) REFERENCES `uzivatele` (`id`) ON DELETE SET NULL;

--
-- Omezení pro tabulku `clanky_rating`
--
ALTER TABLE `clanky_rating`
ADD CONSTRAINT `clanky_rating_ibfk_1` FOREIGN KEY (`clanky_id`) REFERENCES `clanky` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `clanky_rating_ibfk_5` FOREIGN KEY (`uzivatele_id`) REFERENCES `uzivatele` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Omezení pro tabulku `coment_rating`
--
ALTER TABLE `coment_rating`
ADD CONSTRAINT `coment_rating_ibfk_4` FOREIGN KEY (`komentare_id`) REFERENCES `komentare` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `coment_rating_ibfk_5` FOREIGN KEY (`uzivatele_id`) REFERENCES `uzivatele` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Omezení pro tabulku `komentare`
--
ALTER TABLE `komentare`
ADD CONSTRAINT `komentare_ibfk_2` FOREIGN KEY (`uzivatele_id`) REFERENCES `uzivatele` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `komentare_ibfk_3` FOREIGN KEY (`clanky_id`) REFERENCES `clanky` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
