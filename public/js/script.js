$(document).ready(function () {

    /* FB Share Button*/
    $(document).ready(function () {
        $('.fb-share').click(function (e) {
            e.preventDefault();
            window.open($(this).attr('href'), 'fbShareWindow', 'height=450, width=550, top=' + ($(window).height() / 2 - 275) + ', left=' + ($(window).width() / 2 - 225) + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
            return false;
        });
    });
    /* Hodnoceni komentare */
    $(document).on("click", "#rat-up, #rat-down", function () {
        var user_id = $(this).data("user");
        var koment_id = $(this).data("koment");
        var val = ($(this).attr('id') === "rat-up" ? -1 : 1);
        some = $(this);
        request = $.ajax({
//            url: "index.php?ratcom="+koment_id+"&user="+user_id+"&val="+val,
            url: "index.php?p=article",
            data: {ratcom: koment_id, user: user_id, val: val},
            type: "post"
        });
        request.done(function (response, textStatus, jqXHR) {
            data = $.parseJSON(response);
//            console.log(data.val);
            some.parent().find(".ratview").html(data.val);
            if (val === 1) {
                some.parent().find(".down").removeClass("down");
                some.toggleClass("up");
            } else {
                some.parent().find(".up").removeClass("up");
                some.toggleClass("down");
            }
        });
    });

    /* JS validace formulářů*/


    $('#loginForm').bootstrapValidator({
        message: 'Vyplňte prosím',
        excluded: [':disabled'],
        live: 'enabled'
    });

    $('#newArticleForm').bootstrapValidator({
        message: 'Vyplňte prosím',
        excluded: [':disabled'],
        fields: {
            'article[tags]': {
                validators: {
                    notEmpty: {
                        message: 'Nadpis nemůže být prázdný'
                    }
                }
            }
        },
        live: 'enabled'
    });

    $('#registerForm').bootstrapValidator({
        message: 'Vyplňte prosím',
        excluded: [':disabled'],
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            'reg[password]': {
                validators: {
                    identical: {
                        field: 'reg[password2]',
                        message: 'The password and its confirm are not the same'
                    }
                }
            },
            'reg[password2]': {
                validators: {
                    identical: {
                        field: 'reg[password]',
                        message: 'The password and its confirm are not the same'
                    }
                }
            }}
    });

});

// -- > RATING SYSTEM < --- ///
$(document).ready(function () {

    $('#stars').on('starrr:change', function (e, value) {
        console.log("frst" + value);
        $('#count').html(value);
    });

    $('#stars-existing').on('starrr:change', function (e, value) {
        var ths = $(this);
        article = $(this).data("article");
        request = $.ajax({
            url: "index.php?p=article",
            data: {rating: value, article: article},
            type: "post"
        });
        request.done(function (response, textStatus, jqXHR) {
            data = $.parseJSON(response);

            //            console.log(data.val);
            console.log(data.value);
            if ($.isNumeric(data.value)) {
                $('#str-cnt').html(Math.round(data.value * 100) / 100);
            } else {
                $('#str-cnt').html(data.value);
            }

        });

    });
});

// Starrr plugin (https://github.com/dobtco/starrr)
var __slice = [].slice;

(function ($, window) {
    var Starrr;

    Starrr = (function () {
        Starrr.prototype.defaults = {
            rating: void 0,
            numStars: 5,
            change: function (e, value) {
            }
        };

        function Starrr($el, options) {
            var i, _, _ref,
                    _this = this;

            this.options = $.extend({}, this.defaults, options);
            this.$el = $el;
            _ref = this.defaults;
            for (i in _ref) {
                _ = _ref[i];
                if (this.$el.data(i) != null) {
                    this.options[i] = this.$el.data(i);
                }
            }
            this.createStars();
            this.syncRating();
            this.$el.on('mouseover.starrr', 'i', function (e) {
                return _this.syncRating(_this.$el.find('i').index(e.currentTarget) + 1);
            });
            this.$el.on('mouseout.starrr', function () {
                return _this.syncRating();
            });
            this.$el.on('click.starrr', 'i', function (e) {
                return _this.setRating(_this.$el.find('i').index(e.currentTarget) + 1);
            });
            this.$el.on('starrr:change', this.options.change);
        }

        Starrr.prototype.createStars = function () {
            var _i, _ref, _results;

            _results = [];
            for (_i = 1, _ref = this.options.numStars; 1 <= _ref ? _i <= _ref : _i >= _ref; 1 <= _ref ? _i++ : _i--) {
                _results.push(this.$el.append("<i class='fa fa-star-o'></i>"));
            }
            return _results;
        };

        Starrr.prototype.setRating = function (rating) {
            if (this.options.rating === rating) {
                rating = void 0;
            }
            this.options.rating = rating;
            this.syncRating();
            return this.$el.trigger('starrr:change', rating);
        };

        Starrr.prototype.syncRating = function (rating) {
            var i, _i, _j, _ref;

            rating || (rating = this.options.rating);
            if (rating) {
                for (i = _i = 0, _ref = rating - 1; 0 <= _ref ? _i <= _ref : _i >= _ref; i = 0 <= _ref ? ++_i : --_i) {
                    this.$el.find('i').eq(i).removeClass('fa-star-o').addClass('fa-star');
                }
            }
            if (rating && rating < 5) {
                for (i = _j = rating; rating <= 4 ? _j <= 4 : _j >= 4; i = rating <= 4 ? ++_j : --_j) {
                    this.$el.find('i').eq(i).removeClass('fa-star').addClass('fa-star-o');
                }
            }
            if (!rating) {
                return this.$el.find('i').removeClass('fa-star').addClass('fa-star-o');
            }
        };

        return Starrr;

    })();
    return $.fn.extend({
        starrr: function () {
            var args, option;

            option = arguments[0], args = 2 <= arguments.length ? __slice.call(arguments, 1) : [];
            return this.each(function () {
                var data;

                data = $(this).data('star-rating');
                if (!data) {
                    $(this).data('star-rating', (data = new Starrr($(this), option)));
                }
                if (typeof option === 'string') {
                    return data[option].apply(data, args);
                }
            });
        }
    });
})(window.jQuery, window);

$(function () {
    return $(".starrr").starrr();
});

