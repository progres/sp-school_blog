<?php

session_start();
require_once 'Loader.php';
require_once './vendor/autoload.php';
require_once 'config.php';
Loader::addPrefix('controller');
Loader::addPrefix('model');

spl_autoload_register('Loader::load');
MainModel::magicQuotesDisable();
Twig_Autoloader::register();

// zapnout cache
$loader = new Twig_Loader_Filesystem('view');
//  array(     'cache' => 'cache', )
$twig = new Twig_Environment($loader);

$povolene[] = "base";
if ($handle = opendir('controller')) {

    /* This is the correct way to loop over the directory. */
    while (false !== ($entry = readdir($handle))) {
        if ($entry != "." || $entry != "..") {
            $povolene[] = strtolower(substr($entry, 0, -13));
        }
    }
    closedir($handle);
}
//print_r($povolene);

if (isset($_GET['p'])) {
    $p = $_GET['p'];
} else {
    $p = "base";
}

if (!in_array($p, $povolene)) {
    $b = new BasePresenter($twig);
    $b->error($twig);
    exit();
}
if (isset($_GET['action'])) {
    $action = $_GET['action'];
} else {
    $action = "default";
}


$p = ucfirst($p);
$action = ucfirst($action);


$act = "action" . $action;
$hm = $p . "Presenter";

$new = new $hm($twig);
if (isset($_GET['signal'])) {
    $signal = $_GET['signal'];
    $signal = ucfirst($signal);
    $signal = "handle" . $signal;
    $new->$signal();
}
$new->$act();
