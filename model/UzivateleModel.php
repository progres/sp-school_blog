<?php

class UzivateleModel extends MainModel {

    const TABLE = "uzivatele";

    /**
     * Vrátí všechny uživatele
     * @return type
     */
    public function getAllUsers() {
        return $this->getDb()->query("SELECT * FROM " . self::TABLE . " WHERE 1")->fetchAll();
    }

    /**
     * Vrátí uživatlee podle ID
     * @param type $id
     * @return type
     */
    public function getUser($id) {
        return $this->getDb()->query("SELECT * FROM " . self::TABLE . " WHERE id=$id")->fetch();
    }

    /**
     * Vrátí uživatle podle jména
     * @param type $name
     * @return boolean
     */
    public function getUserByName($name) {
        $query = $this->getDb()->prepare("SELECT * FROM " . self::TABLE . " WHERE username=:username");
        $query->bindParam(":username", $name, PDO::PARAM_STR);
        $query->execute();
        if ($query === false) {
            var_dump($this->getDb()->errorInfo());
            return false;
        }
        return $query->fetch(PDO::FETCH_OBJ);
    }

    /**
     * Vrátí uživatle podle jména
     * @param type $name
     * @return boolean
     */
    public function getUserByMail($mail) {
        $query = $this->getDb()->prepare("SELECT * FROM " . self::TABLE . " WHERE email=:email");
        $query->bindParam(":email", $mail, PDO::PARAM_STR);
        $query->execute();
        if ($query === false) {
            var_dump($this->getDb()->errorInfo());
            return false;
        }
        return $query->fetch(PDO::FETCH_OBJ);
    }

    /**
     * Zaregistruje nového uživatele
     * @param array $data
     */
    public function registerUser(array $data) {

        $pass = sha1($data['password']);
        $data['password'] = $pass;
        if ($this->getUserByName($data['username']) == false || $this->getUserByMail($data['email']) == false) {
            $this->DBInsert(self::TABLE, $data);
            return true;
        } else {
            return false;
        }
    }

    /**
     * přihlásí uživatele
     * @param type $username
     * @param type $password
     * @return boolean
     */
    public function login($username, $password) {
        $user = $this->getUserByName($username);
        if (!$user) {
            return false;
        }
        if (sha1($password) == $user->password) {
            return $user->id;
        } else {
            return false;
        }
    }

    /**
     * Přihlásí/zaregistruje a přihlásí uživatele přes FB 
     * @param type $data
     * @return type
     */
    public function fbLogin($data) {
        $email = $data['email'];
        $fb_id = $data['id'];
        $name = $data['name'];
        $date = date("Y-m-d H:i:s");

        $exist = $this->getDb()->query("SELECT * FROM " . self::TABLE . " WHERE email='$email'")->fetchAll();

        if (count($exist) == 0) {
            return $this->getDb()->query("INSERT INTO " . self::TABLE . " (username,email,date,fb_id) "
                            . "VALUES('$name','$email','$date','$fb_id')");
        } else {
            $usr = $this->getDb()->query("SELECT * FROM " . self::TABLE . " WHERE email='$email'")->fetch();
            $id = $usr['id'];
            $this->getDb()->query("UPDATE " . self::TABLE . " SET fb_id='$fb_id',username='$name' WHERE id='$id'");
            return $id;
        }
    }

}
