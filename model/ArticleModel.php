<?php

class ArticleModel extends MainModel {

    const TABLE = "clanky";

    private $tagModel;
    private $clanek_id;
    private $tagy;

    public function __construct() {
        parent::__construct();
        $this->tagModel = new TagModel();
    }

    /**
     * Vraci vsechny artikly 
     * @param type $limit kolik
     * @param type $ofset odkud
     * @return type array Articles
     */
    public function getAllArticles($limit, $ofset) {
        if ($limit == null && $ofset == null) {
            return $this->getDb()->query(
                            "SELECT clanky.*, uzivatele.username, uzivatele.id as user_id, AVG(clanky_rating.rating) as rating  "
                            . "FROM " . self::TABLE . " "
                            . "LEFT JOIN uzivatele ON clanky.uzivatele_id = uzivatele.id "
                            . "LEFT JOIN clanky_rating ON clanky_rating.clanky_id = clanky.id "
                            . "GROUP BY clanky.id "
                            . "ORDER BY clanky.date DESC "
                    )->fetchAll();
        } else {
            return $this->getDb()->query(
                            "SELECT clanky.*, uzivatele.username, uzivatele.id as user_id, count(komentare.id) as cmntcount,"
                            . " AVG(clanky_rating.rating) as rating  "
                            . "FROM " . self::TABLE . " "
                            . "LEFT JOIN komentare ON komentare.clanky_id = clanky.id "
                            . "LEFT JOIN uzivatele ON clanky.uzivatele_id = uzivatele.id "
                            . "LEFT JOIN clanky_rating ON clanky_rating.clanky_id = clanky.id "
                            . "GROUP BY clanky.id "
                            . "ORDER BY clanky.date DESC "
                            . "LIMIT $ofset,$limit "
                    )->fetchAll();
        }
    }

    /**
     * Vrací top X komentovaných artiklů
     * @param type $limit kolik
     * @return type array clanky
     */
    public function topCommented($limit = 5) {
        return $this->getDb()->query(
                        "SELECT clanky.nadpis, clanky.id, count(komentare.id) as cmntcount "
                        . "FROM " . self::TABLE . " "
                        . "RIGHT JOIN komentare ON komentare.clanky_id = clanky.id "
                        . "GROUP BY clanky.id "
                        . "ORDER BY cmntcount DESC "
                        . "LIMIT $limit"
                )->fetchAll();
    }

    /**
     * Vrací top X hodnocených artiklů
     * @param type $limit kolik
     * @return type array clanky
     */
    public function topRated($limit = 5) {
        return $this->getDb()->query(
                        "SELECT clanky.nadpis, clanky.id, AVG(clanky_rating.rating) as rating "
                        . "FROM " . self::TABLE . " "
                        . "LEFT JOIN clanky_rating ON clanky_rating.clanky_id = clanky.id "
                        . "WHERE rating IS NOT NULL "
                        . "GROUP BY clanky.nadpis "
                        . "ORDER BY rating DESC "
                        . "LIMIT $limit"
                )->fetchAll();
    }

    /**
     * Vrací příspěvky které obsahují daný tag
     * @param type $tagId
     * @return array Clanky
     */
    public function getByTag($tagId) {
        $this->getDb()->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
        $query = "SELECT article_n_tagy.*, clanky.*, "
                . "uzivatele.username, uzivatele.id as user_id, count(komentare.id) as cmntcount,  AVG(clanky_rating.rating) as rating "
                . "FROM article_n_tagy "
                . "LEFT JOIN clanky ON article_n_tagy.clanky_id = clanky.id "
                . "LEFT JOIN komentare ON komentare.clanky_id = clanky.id "
                . "LEFT JOIN uzivatele ON clanky.uzivatele_id = uzivatele.id "
                . "LEFT JOIN clanky_rating ON clanky_rating.clanky_id = clanky.id "
                . "WHERE article_n_tagy.tagy_id='$tagId' "
                . "GROUP BY clanky.id "
                . "ORDER BY clanky.date DESC ";

        $sth = $this->getDb()->prepare($query);
        $sth->execute();
        return $sth->fetchAll();
    }

    /**
     * Prida rating k članku
     * @param type $article_id id clanku
     * @param type $user_id id uzivatele
     * @param type $value hodnota (1-5)
     * @return rating clanku
     */
    public function addRating($article_id, $user_id, $value) {
        $data = array("rating" => $value, "date" => date("Y-m-d H:i:s"), "clanky_id" => $article_id, "uzivatele_id" => $user_id);
        $pocet = $this->getDb()->query("SELECT * FROM clanky_rating WHERE uzivatele_id = '$user_id' AND clanky_id='$article_id' ")->fetchAll();
        if (count($pocet) == 0) {

            $this->DBInsert("clanky_rating", $data);
        } else {
            $this->getDb()->query("DELETE FROM clanky_rating WHERE uzivatele_id = '$user_id' AND clanky_id='$article_id' ");
            $this->DBInsert("clanky_rating", $data);
        }
        return $this->getRating($article_id);
    }

    /**
     * Rating clanku
     * @param type $id clanky ID
     * @return rating clanku double
     */
    public function getRating($id) {
        $mmh = $this->getDb()->query("SELECT AVG(clanky_rating.rating) as rating "
                        . "FROM clanky_rating "
                        . "WHERE clanky_id='$id'")->fetch()['rating'];
        if (!isset($mmh)) {
            return 0;
        } else {
            return $mmh;
        }
    }

    /**
     * Vrati clanek podle ID
     * @param type $id id clanku
     * @return array clanek
     */
    public function getArticle($id) {

        return $this->getDb()->query(
                        "SELECT clanky.*, uzivatele.username, uzivatele.id as user_id, count(komentare.id) as cmntcount "
                        . "FROM " . self::TABLE . " "
                        . "LEFT JOIN komentare ON komentare.clanky_id = clanky.id "
                        . "LEFT JOIN uzivatele ON clanky.uzivatele_id = uzivatele.id "
                        . "WHERE clanky.id = '$id' "
                )->fetch();
    }

    /**
     * Vraci clanky uzivatele s ID
     * @param type $user_id user id
     * @return array clanky
     */
    public function getUserArticles($user_id) {
        return $this->getDb()->query(
                        "SELECT clanky.nadpis, clanky.date, clanky.id, count(komentare.id) as cmntcount, AVG(clanky_rating.rating) as rating  " //HBADS
                        . "FROM " . self::TABLE . " "
                        . "LEFT JOIN komentare ON komentare.clanky_id = clanky.id "
                        . "LEFT JOIN clanky_rating ON clanky_rating.clanky_id = clanky.id "
                        . "WHERE clanky.uzivatele_id='$user_id' "
                        . "GROUP BY clanky.id "
                        . "ORDER BY clanky.date DESC "
                )->fetchAll();
    }

    /**
     * Vytvoří nový článek
     * @param type $data data
     */
    public function newArticle($data) {
        $tagy = $data['tags'];

        unset($data['tags']);


        $this->clanek_id = $this->DBInsert(self::TABLE, $data);
        MainModel::explode($tagy);
        $this->processTagy($tagy);
    }

    /**
     * Smaze všechny tagy članku
     * @param type $id
     */
    public function deleteArticleTags($id) {
        $this->getDb()->query("DELETE FROM article_n_tagy WHERE clanky_id ='$id'");
    }

    /**
     * Upraví článek
     * @param type $id jaký článek 
     * @param type $data nová data
     */
    public function editArticle($id, $data) {
        $this->clanek_id = $id;
        $tagy = $data['tags'];
        MainModel::explode($tagy);
        $this->deleteArticleTags($this->clanek_id);
        $this->processTagy($tagy);
        unset($data['tags']);
        $data = array_filter($data, function ($value) {
            return null !== $value;
        });
        $query = "UPDATE " . self::TABLE . " SET";
        $values = array();

        foreach ($data as $name => $value) {
            $query .= ' ' . $name . ' = :' . $name . ',';
            $values[':' . $name] = $value;
        }
        $query = substr($query, 0, -1);
        $query .= " WHERE id='$id' " . ';';

        $sth = $this->getDb()->prepare($query);
        $sth->execute($values);
    }

    /**
     * Zpracuje tagy k članku přiřadí je do n:m tabulky
     * @param array $tags tagy
     */
    public function processTagy(array $tags = null) {
        if (!isset($tags)) {
            $tags = $this->tagy;
        }
        $current_tags = array();
        foreach ($this->tagModel->findAll() as $tag) {
            $current_tags[] = $tag->nazev;
        }
        foreach (array_unique(array_diff(array_map('strtolower', $tags), array_map('strtolower', $current_tags))) as $newtag) {
            $this->tagModel->insertTag(array(
                'nazev' => $newtag
            ));
        }

        for ($i = 0; $i < count($tags); $i++) {
            $this->tagModel->attachToArticle(
                    $this->tagModel->getIdByName($tags[$i]), $this->clanek_id
            );
        }
    }

    /**
     * Odstraní článek podle ID 
     * @param type id clanku
     */
    public function delete($id) {
        $this->getDb()->query("DELETE FROM " . self::TABLE . " WHERE id='$id'");
    }

}
