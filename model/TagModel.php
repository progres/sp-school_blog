<?php

class TagModel extends MainModel {

    const TABLE = "tagy";

    public function findAll() {
        return $this->getDb()->query("SELECT *, COUNT(article_n_tagy.tagy_id) as cetnost "
                        . "FROM " . self::TABLE . " "
                        . "LEFT JOIN article_n_tagy ON article_n_tagy.tagy_id = tagy.id "
                        . "GROUP BY tagy.nazev "
                        . "ORDER BY tagy.nazev "
                )->fetchAll(PDO::FETCH_OBJ);
    }

    /**
     * Vrací pole názvů všech tagů
     * @return type
     */
    public function findAllNames() {
        return $this->getDb()->query("SELECT nazev FROM " . self::TABLE . " WHERE 1"
                )->fetchAll();
    }

    /**
     * Vloží nový tag
     * @param type $tag
     * @return type
     */
    public function insertTag($tag) {
        return $this->DBInsert(self::TABLE, $tag);
    }

    /**
     * Najde tag podle ID
     * @param type $id
     * @return type
     */
    public function find($id) {
        return $this->getDb()->query("SELECT * FROM " . self::TABLE . " WHERE id='$id'")->fetch(PDO::FETCH_OBJ);
    }

    /**
     * Vrátí ID tagu podle názvu
     * @param type $tagname
     * @return type
     */
    public function getIdByName($tagname) {
        return $this->getDb()->query("SELECT * FROM " . self::TABLE . " WHERE nazev='$tagname'")->fetch(PDO::FETCH_OBJ)->id;
    }

    /**
     * přiřadí tag ke článku
     * @param type $tag_id
     * @param type $article_id
     */
    public function attachToArticle($tag_id, $article_id) {
        $this->DBInsert("article_n_tagy", array(
            'clanky_id' => $article_id,
            'tagy_id' => $tag_id
        ));
    }

    /**
     * Vrátí tagy článku
     * @param type $clanky_id
     * @return type
     */
    public function articleTags($clanky_id) {
        return $this->getDb()->query(
                        "SELECT article_n_tagy.*, tagy.nazev, tagy.id "
                        . "FROM article_n_tagy "
                        . "LEFT JOIN tagy ON  tagy.id = article_n_tagy.tagy_id "
                        . "WHERE clanky_id='$clanky_id' "
                )->fetchAll(PDO::FETCH_OBJ);
    }

}
