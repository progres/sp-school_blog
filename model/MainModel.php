<?php


class MainModel {

    public $connection = null;

    public function __construct() {
        $this->Connect();
    }

    public function __destruct() {
        $this->Disconnect();
    }

    /**
     * Připojí přes PDO do DB
     */
    private function Connect() {
        try {
            $options = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',);
            $this->connection = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_DATABASE_NAME . "", DB_USER_LOGIN, DB_USER_PASSWORD, $options);
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    /**
     * Vrací instanci PDO
     * @return type
     */
    public function getDb() {
        return $this->connection;
    }

    /**
     * Vynuluje připojení
     */
    public function Disconnect() {
        $this->connection = null;
    }

    /**
     * Zakáže magicQuotes
     */
    public static function magicQuotesDisable() {
        // odstranění ošetření dat způsobeného direktivou magic_quotes_gpc
        if (get_magic_quotes_gpc()) {
            $process = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
            while (list($key, $val) = each($process)) {
                foreach ($val as $k => $v) {
                    unset($process[$key][$k]);
                    if (is_array($v)) {
                        $process[$key][stripslashes($k)] = $v;
                        $process[] = &$process[$key][stripslashes($k)];
                    } else {
                        $process[$key][stripslashes($k)] = stripslashes($v);
                    }
                }
            }
            unset($process);
        }
    }

    /**
     * Pomocná funkce, v hezčím formátu vydebugguje proměnnou 
     * @param type $var
     */
    public static function dump($var) {
        echo "<hr>";
        echo "<pre>";
        var_dump($var);
        echo "</pre>";
        echo "<hr>";
    }

    /**
     * 
     * Pridat polozku do DB - zakladni verze bez mysl fci typu now().
     * @param unknown_type $table_name
     * @param array $item - musi byt ve stejne podobe jako DB.
     * 
     * */
    public function DBInsert($table_name, $item) {
        // MySql
        $mysql_pdo_error = false;

        // SLOZIT TEXT STATEMENTU s otaznikama
        $insert_columns = "";
        $insert_values = "";

        if ($item != null)
            foreach ($item as $column => $value) {
                // pridat carky
                if ($insert_columns != "")
                    $insert_columns .= ", ";
                if ($insert_columns != "")
                    $insert_values .= ", ";

                $insert_columns .= "`$column`";
                $insert_values .= "?";
            }

        // 1) pripravit dotaz s dotaznikama
        $query = "insert into `$table_name` ($insert_columns) values ($insert_values);";

        // 2) pripravit si statement
        $statement = $this->connection->prepare($query);

        // 3) NAVAZAT HODNOTY k otaznikum dle poradi od 1
        $bind_param_number = 1;

        if ($item != null)
            foreach ($item as $column => $value) {
                $statement->bindValue($bind_param_number, $value);  // vzdy musim dat value, abych si nesparoval promennou (to nechci)
                $bind_param_number ++;
            }

        // 4) provest dotaz
        $statement->execute();

        // 5) kontrola chyb
        $errors = $statement->errorInfo();
        //printr($errors);

        if ($errors[0] + 0 > 0) {
            // nalezena chyba
            $mysql_pdo_error = true;
        }

        // 6) nacist ID vlozeneho zaznamu a vratit
        if ($mysql_pdo_error == false) {
            $item_id = $this->connection->lastInsertId();
            return $item_id;
        } else {
            $this->dump($this->connection->errorInfo());
            $this->dump($errors);
//            echo "SQL dotaz: $query";
        }
    }

    /**
     * Safe parse input separated by comma
     * @param array $data
     */
    public static function explode(&$data) {
        $data = trim($data, ' ,');
        $data = explode(',', $data);
        array_walk($data, function (&$value, $key) {
            $value = trim($value, ' ,');
        });
        $data = array_values(array_filter(array_unique($data)));
    }

    /**
     * Escapuje polozky pole
     * @param array $pole
     */
    public function escapujHTML(array &$pole) {
        foreach ($pole as &$polozka) {
            $polozka = (htmlspecialchars($polozka));
        }
    }

}
