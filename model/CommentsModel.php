<?php

class CommentsModel extends MainModel {

    const TABLE = "komentare";
    /**
     * Vrátí všechny komentáře k článku
     * @param type $article_id
     * @return type
     */
    public function getAll($article_id) {

        $query1 = "SELECT komentare.*, uzivatele.username, uzivatele.id as user_id, "
                . "SUM(coment_rating.value) as rating "
                . "FROM " . self::TABLE . " "
                . "LEFT JOIN coment_rating ON komentare.id = coment_rating.komentare_id "
                . "LEFT JOIN uzivatele ON komentare.uzivatele_id = uzivatele.id "
                . "WHERE clanky_id ='$article_id' "
                . "GROUP BY komentare.id "
                . "ORDER BY komentare.date DESC ";
        return $this->getDb()->query($query1)->fetchAll();
    }

    /**
     * Přidá komentář k článku 
     * @param type $data
     */
    public function addComment($data) {
        $this->DBInsert(self::TABLE, $data);
    }

    /**
     *  Smaže komentář
     * @param type $id
     */
    public function delete($id) {
        $this->getDb()->query("DELETE FROM " . self::TABLE . " WHERE id='$id'");
    }

    /**
     * Přidá hodnocení komentáře
     * @param array $data
     * @return
     */
    public function addRating(array $data) {
        $uid = $data['uzivatele_id'];
        $kid = $data['komentare_id'];
        $pocet = $this->getDb()->query("SELECT * FROM coment_rating WHERE uzivatele_id = '$uid' AND komentare_id='$kid' ")->fetchAll();
        if (count($pocet) == 0) {
            return $this->DBInsert("coment_rating", $data);
        } else {
            $this->getDb()->query("DELETE FROM coment_rating WHERE uzivatele_id = '$uid' AND komentare_id='$kid' ");
            return null;
        }
    }

    /**
     * Vrátí hodnocení komentáře
     * @param type $id_coment
     * @return  null : int hodnocení
     */
    public function getRating($id_coment) {
        $mmh = $this->getDb()->query("SELECT SUM(coment_rating.value) as rating "
                        . "FROM coment_rating "
                        . "WHERE komentare_id='$id_coment'")->fetch()['rating'];
        if (!isset($mmh)) {
            return 0;
        } else {
            return $mmh;
        }
    }

    /**
     * Vrátí hodnocení komentáře, kterou uživatel komentáři udělil
     * @param type $id_user id uživatele
     * @param type $id_coment id komentáře
     * @return int hodnocení
     */
    public function userHasComment($id_user, $id_coment) {
        $mmh = $this->getDb()->query("SELECT value FROM coment_rating "
                        . "WHERE uzivatele_id='$id_user' AND komentare_id='$id_coment'")->fetch();
        if (count($mmh) != 0) {
            return $mmh['value'];
        }
    }

}
