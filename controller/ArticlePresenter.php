<?php

class ArticlePresenter extends BasePresenter {

    private $id;
    public $twig;

    public function __construct($twig) {
        parent::__construct($twig);
        $this->twig = $twig;
        $this->template = $twig->loadTemplate("article.twig");
    }

    /**
     * Default akce - zobrazi clanek podle ID
     */
    public function actionDefault() {
        if (isset($_GET['id'])) {
            $this->id = $this->getIdNameId($_GET['id']);
            if (isset($_POST['comments'])) {
                $data = $_POST['comment'];
                $data['uzivatele_id'] = $this->idLogged();
                $data['clanky_id'] = $this->id;
                $data['date'] = date("Y-m-d H:i:s");
                $this->komentsModel->addComment($data);
                header("Location: index.php?p=article&id=$this->id");
            }
            $article = $this->articleModel->getArticle($this->id);
            if ($article['id'] == null) {
                $this->template = $this->twig->loadTemplate("404.twig");
                echo $this->template->render($this->templateData);
                exit();
            }
            $this->templateData['ratings'] = $this->articleModel->getRating($this->id);
            $this->templateData['logged'] = $this->whoLoggedIn();
            $this->templateData['idgged'] = $this->idLogged();
            $this->templateData['article'] = $article;
            $this->templateData['komentsModel'] = $this->komentsModel;
            $this->templateData['comments'] = $this->komentsModel->getAll($this->id);
            $this->templateData['tags'] = $this->tagModel->articleTags($this->id);
            echo $this->template->render($this->templateData);
        }
        /* hodnocení komentáře */
        if (isset($_POST['ratcom'])) {
            $komentId = $_POST['ratcom'];
            $userId = $_POST['user'];
            $val = $_POST['val'];
            $data = array("value" => $val, "uzivatele_id" => $userId, "komentare_id" => $komentId);
            $id = $this->komentsModel->addRating($data);
            echo json_encode(array("val" => $this->komentsModel->getRating($komentId), "state" => $val));
        }
        /* Hodnoceni článku */
        if (isset($_POST['rating'])) {
            $value = $_POST['rating'];
            $article = $_POST['article'];
            if ($this->idLogged() == 0) {
                echo json_encode(array("value" => "Pro hodnoceni se prishlaste"));
            } else {
                echo json_encode(array("value" => $this->articleModel->addRating($article, $this->idLogged(), $value)));
            }
        }
    }

    /**
     * Signál smazání kometáře
     */
    public function handleKdelete() {
        $this->id = $_GET['id'];
        if (isset($_GET['kid'])) {
            $id = $_GET['kid'];
            $this->komentsModel->delete($id);
            $this->setMessage("Komentář byl smazán");
            header("Location: index.php?p=article&id=$this->id");
        }
    }

}
