<?php

class TagPresenter extends BasePresenter {

    public function __construct($twig) {
        parent::__construct($twig);
        $this->template = $twig->loadTemplate("tag.twig");
    }

    /**
     * Zobrazi stranku s prispevky daneho tagu 
     */
    public function actionDefault() {
        if (isset($_GET['tag'])) {
            $id = $this->getIdNameId($_GET['tag']);
            $this->templateData['tag'] = $this->tagModel->find($id);
            $this->templateData['articles'] = $this->articleModel->getByTag($id);
            echo $this->template->render($this->templateData);
        }
    }

}
