<?php

class LoginPresenter extends BasePresenter {

    public $uzivateleModel;

    public function __construct($twig) {
        parent::__construct($twig);
        $this->uzivateleModel = new UzivateleModel();
        $fbUrl = $this->facebook->getLoginUrl(array(
            'scope' => 'email',
            'redirect_uri' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]&p=login&action=fb"// absolute
        ));
        $this->templateData['facebookLogin'] = $fbUrl;
    }

    /**
     * defaultAction - prihlasi uzivatele
     */
    public function actionDefault() {
        $this->template = $this->twig->loadTemplate("login.twig");
        $this->templateData['page'] = "login";
        if (isset($_POST['login'])) {
            $data = $_POST['log'];
            if ($this->necoPrazdne($data)) {
                $this->setMessage("Vyplnte všechy údaje", "error");
                header('Location: index.php?p=login');
            } else {
                $id = $this->uzivateleModel->login($data['username'], $data['password']);

                if ($id) {
                    $_SESSION['user']['name'] = $data['username'];
                    $_SESSION['user']['time'] = date("Y-m-d H:i:s");
                    $_SESSION['user']['id'] = $id;
                    $this->setMessage("Přihlašeno");
                    header('Location: index.php');
                } else {
                    $this->setMessage("Přihlašovací údaje nejsou správné", "error");
                    header('Location: index.php?p=login');
                }
            }
        }
        echo $this->template->render($this->templateData);
    }

    /**
     * Prihlasi uzivatele pres FB
     */
    public function actionFb() {

        $user = $this->facebook->getUser();

        if ($user) {
            $me = $this->facebook->api("/me");
            $logoutUrl = $this->facebook->getLogoutUrl();
        } else {
            $loginUrl = $this->facebook->getLoginUrl();
            header('Location: ' . $loginUrl);
        }
        $auth = $this->uzivateleModel->fbLogin($me);
        if ($auth) {
            $_SESSION['user']['name'] = $me['name'];
            $_SESSION['user']['time'] = date("Y-m-d H:i:s");
            $_SESSION['user']['id'] = $auth;
            $this->setMessage("Přihlašeno");
        } else {
            $this->setMessage("Vyskytla se chyba");
        }

        header('Location: index.php');
    }

    /**
     * Odhlasi uzivatele
     */
    public function actionLogout() {
        unset($_SESSION['user']);
        $this->setMessage("Odhlášení proběhlo úspěšně");
        header('Location: index.php?f=odhlasen');
    }

    /**
     * Zaregistruje uzivatele
     */
    public function actionRegister() {
        $this->template = $this->twig->loadTemplate("register.twig");
        $this->templateData['page'] = "register";
        if (isset($_POST['register'])) {
            $data = $_POST['reg'];

            if ($data['password'] != $data['password2']) {
                $this->setMessage("Hesla se nerovnají", "error");
                header('Location: index.php?p=login&action=register');
            } elseif ($this->necoPrazdne($data)) {
                $this->setMessage("Vyplnte všechny údaje", "error");
                header('Location: index.php?p=login&action=register');
            } else {

                unset($data['password2']);
                $data['date'] = date("Y-m-d H:i:s");
                if ($this->uzivateleModel->registerUser($data)) {
                    $this->setMessage("Registrace proběhla úspěšně");
                    header('Location: index.php');
                } else {
                    $this->setMessage("Uživatel s tímto jmenem již existuje");
                    header('Location: index.php?p=login&action=register');
                }
            }
        }
        echo $this->template->render($this->templateData);
    }

}
