<?php

class BasePresenter {

    public $twig;
    public $template;
    public $templateData;
    public $komentsModel;
    public $articleModel;
    public $tagModel;
    public $currentPage = 1;
    public $facebook;

    public function __construct($twig) {
        $this->articleModel = new ArticleModel();
        $this->tagModel = new TagModel();
        $this->komentsModel = new CommentsModel();
        $this->twig = $twig;
        $this->template = $twig->loadTemplate("homepage.twig");
        $this->templateData = array("user" => $this->whoLoggedIn(), "messages" => $this->getMessages());
        $this->templateData['alltags'] = $this->tagModel->findAll();
        $this->templateData['topcommented'] = $this->articleModel->topCommented();
        $this->templateData['toprated'] = $this->articleModel->topRated();
        $this->templateData['tagModel'] = $this->tagModel;
        $this->facebook = new Facebook(array(
            'appId' => '1920291188110177',
            'secret' => 'c6ec1970025e011e986d23571e4a0432',
        ));
        $this->templateData['url'] = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    }

    /**
     * 
     * @param type $twig Twig Instance
     * vyrkesli do obsahu 404 Error page
     */
    public function error($twig) {
        $this->template = $twig->loadTemplate("404.twig");
        echo $this->template->render($this->templateData);
    }

    /**
     * Default action zobrazi hlavní stránku
     */
    public function actionDefault() {
        $articles = $this->articleModel->getAllArticles(null, null);
        $totalItems = count($articles);
        if (isset($_GET['page'])) {
            $this->currentPage = $_GET['page'];
        }
        $itemsPerPage = 5;
        $this->currentPage;
        $urlPattern = 'index.php?page=(:num)';
        $paginator = new JasonGrimes\Paginator($totalItems, $itemsPerPage, $this->currentPage, $urlPattern);

        $offset = ($paginator->getCurrentPage() - 1) * $itemsPerPage;
        $this->templateData['tagModel'] = $this->tagModel;
        $this->templateData['articles'] = $this->articleModel->getAllArticles($itemsPerPage, $offset);

        $this->templateData['paginator'] = $paginator;
        echo $this->template->render($this->templateData);
    }

    /**
     * Vraci true/false pokud je kdokoliv prihlasen
     * @return boolean je uzivatel prihlasen?
     */
    public function whoLoggedIn() {
        if (isset($_SESSION['user'])) {
            return $_SESSION['user']['name'];
        }
        return false;
    }

    /**
     * Vraci id prihlaseneho uzivatele
     * @return ID prihlaseneho uzivatele, nebo Null
     */
    public function idLogged() {
        if (isset($_SESSION['user'])) {
            return $_SESSION['user']['id'];
        }
        return false;
    }

    /**
     * vraci celeho přihlaseneho uzivatele
     * @return User 
     */
    public function isLoggedIn() {
        return (isset($_SESSION['user']));
    }

    /**
     * Nastaví informační hlášku pro uživatele
     * @param type $message hláška
     * @param type $error jedna se o error
     */
    public function setMessage($message, $error = null) {
        $_SESSION['messages'][] = $message;
    }

    /**
     * Vrati string informacnich hlasek pro uzivatele oddelenych <br>
     * @return string Hlasky
     */
    public function getMessages() {
        $return = "";
        if (isset($_SESSION['messages'])) {
            foreach ($_SESSION['messages'] as $mess) {
                $return .= $mess . "<br>";
            }
            unset($_SESSION['messages']);
        }

        return $return;
    }

    /**
     * Proveri, zdali je v poli nejaky prazdny prvek
     * @param array $data pole
     * @param array $muze_prazdne key pole ktery muze byt prazdny
     * @return boolean vraci true pokud je neco prazdne
     */
    public function necoPrazdne(array $data, array $muze_prazdne = null) {

        foreach ($data AS $key => $value) {
            if (empty($value)) {
                if (!in_array($key, $muze_prazdne)) {
                    return true;
                }
            }
        }
    }

    /**
     * z formatu id-nazev v url vrati ID
     * @param type $string url-format-id+name
     * @return type ID 
     */
    public function getIdNameId($string) {
        $pole = explode("-", $string);
        return (int) $pole[0];
    }

}
