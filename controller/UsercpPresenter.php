<?php

class UsercpPresenter extends BasePresenter {

    public $articleModel;

    public function __construct($twig) {
        parent::__construct($twig);
        if (!$this->isLoggedIn()) {
            header('Location: index.php');
        }
        $this->articleModel = new ArticleModel();
        $this->template = $twig->loadTemplate("usercp.twig");
    }

    /**
     * Pokud v url existuje ID, tak do sablony poslem infomace o clanku
     * => editace clanku
     */
    public function handleUpravit() {
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            $artikl = $this->articleModel->getArticle($id);
            $tags = $this->tagModel->articleTags($id);
            $this->templateData['article'] = $artikl;
            $this->templateData['tags'] = $tags;
        }
    }

    /**
     * DefaultAction => Vytvorit novy clanek
     */
    public function actionDefault() {
        $this->templateData['page'] = "newa";
        if (isset($_POST['newarticle'])) {
            $data = $_POST['article'];
            if ($this->necoPrazdne($data)) {
                $this->templateData['article'] = $data;
                $this->setMessage("Vyplnte nadpis a obsah", "error");
                header("Location: index.php?p=usercp");
            } else {
                if (isset($_GET['id'])) {
                    $this->articleModel->editArticle($_GET['id'], $data);
                    $this->setMessage("Příspěvek upraven");
                    header("Location: index.php");
                } else {

                    $data['date'] = date("Y-m-d H:i:s");
                    $data['uzivatele_id'] = $this->idLogged();
                    $this->articleModel->newArticle($data);
                    $this->setMessage("Příspěvek vytvořen");
                    header("Location: index.php");
                }
            }
        }
        $tagy = $this->tagModel->findAllNames();
        if (!empty($tagy)) {
            foreach ($tagy as $tag) {
                $tgs[] = $tag['nazev'];
            }
            $this->templateData['tagy'] = '"' . implode('","', $tgs) . '"';
        }
        $this->templateData['sekce'] = "new";
        echo $this->template->render($this->templateData);
    }

    /**
     * Zobrazi seznam članků patřících uživateli
     */
    public function actionPrehled() {
        $this->templateData['myArticles'] = $this->articleModel->getUserArticles($this->idLogged());
        $this->templateData['sekce'] = "prehled";
        echo $this->template->render($this->templateData);
    }

    /**
     * Smaže článek
     */
    public function handleSmazat() {
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            $this->articleModel->delete($id);
        }
    }

}
